package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(@PathVariable("sku") sku: String): ResponseEntity<ProductResponse> {
        logger.info("=>findProductsBySku() $sku")
        val product = productService.findProductBySku(sku)
        return if (product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products/{skus}", produces = ["application/json;charset=utf-8"])
    fun findProductsByListOfSku(@PathVariable("skus") listOfSku: List<String>): ResponseEntity<List<ProductResponse>> {
        logger.info("=>findProductsByListOfSku() $listOfSku")
        val products = productService.findProductsByListOfSku(listOfSku)
        return if (products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PutMapping("/product/{sku}", consumes = arrayOf(APPLICATION_JSON_VALUE))
    fun updateProductById(@PathVariable("sku") sku: String, @RequestBody productResponse: ProductResponse): ProductResponse =
            productService.updateProductBySku(sku, productResponse)

    @PostMapping("/product/", consumes = arrayOf(APPLICATION_JSON_VALUE))
    fun addProduct(@RequestBody productEntity: ProductEntity): ProductEntity =
            productService.addProduct(productEntity)

}
