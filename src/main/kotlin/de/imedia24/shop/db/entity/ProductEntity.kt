package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
data class ProductEntity(
        @Id
        @Column(name = "sku", nullable = false)
        val sku: String = "",

        @Column(name = "name", nullable = false)
        var name: String = "",

        @Column(name = "description")
        val description: String? = null,

        @Column(name = "price", nullable = false)
        val price: BigDecimal = 1.toBigDecimal(),

        @Column(name = "stock_location", nullable = false)
        val stock_location: String = "",

        @UpdateTimestamp
        @Column(name = "created_at", nullable = false)
        val createdAt: ZonedDateTime = LocalDateTime.now().atZone(ZoneId.systemDefault()),

        @UpdateTimestamp
        @Column(name = "updated_at", nullable = false)
        val updatedAt: ZonedDateTime = LocalDateTime.now().atZone(ZoneId.systemDefault())


)
