package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneId

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findById(sku).get().toProductResponse()
    }

    fun findProductsByListOfSku(listOfSku: List<String>): List<ProductResponse>? {
        return productRepository.findAllById(listOfSku).map { productEntity -> productEntity.toProductResponse() }
    }

    fun updateProductBySku(sku: String, product: ProductResponse): ProductResponse {
        var updatedProduct: ProductEntity? = null
        try {
            productRepository.findById(sku).map { previousData ->
                updatedProduct = previousData.copy(
                        name = product.name,
                        description = product.description,
                        price = product.price,
                        stock_location = product.stock_location,
                        updatedAt = LocalDateTime.now().atZone(ZoneId.systemDefault())
                )
                productRepository.save(updatedProduct!!)
            }
        } catch (e: Exception) {
            throw e
        }
        return updatedProduct!!.toProductResponse()
    }

    fun addProduct(product: ProductEntity): ProductEntity = productRepository.save(product)

}
