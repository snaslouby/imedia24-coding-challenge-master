package de.imedia24.shop

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class ShopApplicationTests @Autowired constructor(
        val productRepository: ProductRepository,
        val productService: ProductService) {
    /**
     * Test of add product and of find product by Sku
     */
    @Test
    fun findBySkuTest() {
        var productEntity: ProductEntity = ProductEntity("test1", "test1", "test1", 1.toBigDecimal(), "test1")
        productService.addProduct(productEntity)
        var productEntityBySearch: ProductResponse? = productService.findProductBySku("test1")
        assertThat(productEntity.toProductResponse()).isEqualTo(productEntityBySearch)
    }

    /**
     * Test of add product and of find product by list of Sku
     */
    @Test
    fun findByListSkuTest() {
        //Add to database
        var productEntity1: ProductEntity = ProductEntity("test1", "test1", "test1", 1.toBigDecimal(), "test1")
        productService.addProduct(productEntity1)
        var productEntity2: ProductEntity = ProductEntity("test2", "test2", "test2", 1.toBigDecimal(), "test2")
        productService.addProduct(productEntity2)
        //List of local products added manually
        var localProductsList: List<ProductResponse> = listOf(productEntity1.toProductResponse(), productEntity2.toProductResponse())
        //List of function that gets data from database
        var productEntityBySearchList: List<ProductResponse>? = productService.findProductsByListOfSku(listOf("test1", "test2"))

        assertThat(localProductsList).isEqualTo(productEntityBySearchList)
    }

    /**
     * Test of add product and of update product
     */
    @Test
    fun updateProductTest() {
        var productEntity: ProductEntity = ProductEntity("test1", "test1", "test1", 1.toBigDecimal(), "test1")
        productService.addProduct(productEntity)

        var productEntityUpdated: ProductEntity = ProductEntity("test1", "test1_Updated", "test1_Updated", 1.toBigDecimal(), "test1_Updated")

        var productEntityBySearch: ProductResponse? = productService.updateProductBySku("test1", productEntityUpdated.toProductResponse())
        //Compare updated product with product in database - Expected Result: Equal
        assertThat(productEntityUpdated.toProductResponse()).isEqualTo(productEntityBySearch)
        //Compare initial product with product in database - Expected Result: Not Equal
        assertThat(productEntity.toProductResponse()).isNotEqualTo(productEntityBySearch)
    }

    @Test
    fun contextLoads() {
    }

}
